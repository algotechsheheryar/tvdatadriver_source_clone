const get_user_from_token = require("./../helper/jwtvalidator.helper");
const User = require("./../models/user.model");
const WeeklySymbols = require('./../models/weeklysymbols.model')
const StockQuotes = require('./../models/stockquotes.model')

module.exports = async (req, res, next) => {
  // console.log('2')
  // console.log(res.geade)
  const config = require("./../config/keys.config")();
  config
    .then(config => {
      const models = {
        UserModel: User,
        WeeklySymbolsModel: WeeklySymbols,
        StockQuotesModel: StockQuotes
      }
      const B_token = req.headers.authorization;
      if (B_token) {
        const _id = get_user_from_token(B_token);
        User.findById({ _id: _id }).then(user => {
          req["config"] = config;
          if (user && user.is_tradier_linked) {
            req.config.tradierToken = user.tradier_access_token;
            req.account_no = req.headers.tradier_account;
            req["tradier_url"] =
              user.account_type === "paper"
                ? "https://sandbox.tradier.com/v1"
                : "https://api.tradier.com/v1";
            console.log(req.tradier_url)
          } else {
            req["tradier_url"] = "https://api.tradier.com/v1";
          }
          req['models'] = models
          next();
        });
      } else {
        req["config"] = config;
        req["tradier_url"] = "https://api.tradier.com/v1";
        req['models'] = models
        next();
      }

      
    })
    .catch(e => console.log("error with config", e));
};
