const TradierStrategy = require("passport-tradier");
const config = require("./../config/keys.config")();

module.exports = passport => {
  passport.serializeUser((user, done) => done(null, user));
  passport.deserializeUser((user, done) => done(null, user));
  config.then(config => {
    passport.use(
      new TradierStrategy(
        {
          clientID: config.refreshToken.clientID,
          clientSecret: config.refreshToken.secret,
          callbackURL: "http://localhost:5000/api/auth/tradier/callback",
          passReqToCallback: true
        },
        (req, accessToken, refreshToken, profile, done) => {
          const user = {
            accessToken,
            refreshToken,
            name: profile.name,
            id: profile.id
          };
          done(null, user)
          
        }
      )
    );
  });
};
