const JwtStrategy = require("passport-jwt").Strategy,
  ExtractJwt = require("passport-jwt").ExtractJwt;
const axios = require("axios");

// validate if token is genrated within 24 hrs
const token_validator = created_at => {
  const ONE_HOUR = 60 * 60 * 1000;
  const _24_hour = ONE_HOUR * 24;
  return new Date() - created_at < _24_hour;
};
module.exports = (passport, User) => {
  const config = require("./../config/keys.config")();
  var opts = {};
  // getting coinfig
  config.then(config => {
    //extracting jwt from req headers
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = "secret";
    passport.use(
      //defining strategy
      new JwtStrategy(opts, (jwt_payload, done) => {
        // querying Mongodb for _id in jwt
        User.findOne({ _id: jwt_payload._id }).then(user => {
          // return err if user not exist
          if (!user) return done(null, { err: "user not found" });
          else if (user) {
            // validating user tradier token
            const is_tradier_token_valid = token_validator(
              new Date(user.tradier_token_created_at)
            );
            // requesting new token if not valid
            if (
              user.is_tradier_linked &&
              !is_tradier_token_valid &&
              user.account_type !== "paper"
            ) {
              axios({
                method: "post",
                url: `https://api.tradier.com/v1/oauth/refreshtoken?grant_type=refresh_token&refresh_token=${user.tradier_refresh_token}`,
                headers: {
                  Authorization: "Bearer " + config.tradierToken
                },
                //basic http auth required by tradier
                auth: {
                  username: config.refreshToken.clientID,
                  password: config.refreshToken.secret
                }
              })
                .then(res => {
                  // updating user access token
                  User.findOneAndUpdate(
                    { _id: user._id },
                    {
                      tradier_access_token: res.data.access_token,
                      tradier_token_created_at: Date.now(),
                      tradier_refresh_token: res.data.refresh_token
                    }
                  )
                    .then(user => done(null, user))
                    .catch(err => done(null, user));
                })
                .catch(e => {
                  console.log(e.response.data);
                  done(null, user);
                });
            } else return done(null, user);
          }
        });
      })
    );
  });
};
