const chart_router = require("../routes/chart.router");
const options_router = require("../routes/options.router");
const order_ticket_router = require("../routes/order-ticket.router");
const auth_router = require("../routes/auth.router");
const account_router = require("../routes/account.router");
module.exports = app => {
  app.use("/api/chart", chart_router);
  app.use("/api/options", options_router);
  app.use("/api/order_ticket", order_ticket_router);
  app.use("/api/auth", auth_router);
  app.use("/api/account", account_router);
};
