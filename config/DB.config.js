const mongoose = require('mongoose')

module.exports = (DB, name) => {
    console.log(DB)
    mongoose.connect(
        `mongodb://${DB.username}:${DB.password}@${DB.host}:${DB.port}/${name}?authSource=admin`,
        { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false }
    ).then(() => console.log("DB Connected " + name))
     .catch(e => console.log("db error", e.message));
};