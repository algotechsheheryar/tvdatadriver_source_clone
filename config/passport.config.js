const passport = require("passport");
const User = require('./../models/user.model')

module.exports = app => {
  //requiring and initializing passport-tradier-strategy
  require("./../Passport/passport-tradier-strategy")(passport);
  // requiring and initializing passport-jwt-strategy
  require("./../Passport/passport-jwt-strategy")(passport, User);
  // initializing passport.js to work with our app
  app.use(passport.initialize());
};
