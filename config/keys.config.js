const axios = require("axios");
const config_server = "https://algopti-config.herokuapp.com/api/dev/get_config";

// const config_server = "http://localhost:4000/api/dev/get_config";
const server_access_token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZTg2NTM0MWE0YTliMzAwMTdhMTZhNTciLCJ1c2VybmFtZSI6IkhhbW1hZCIsImlhdCI6MTU4NTg2MTQ0MX0.wg9VUBE6B_0CZ713GqnDpj5hKQS4wCcb34xPGOn9QDI";

module.exports = () => {
  var config = null;
  const set_config = data => {
    config = data;
  };
  return axios({
    method: "get",
    url: config_server,
    headers: {
      Authorization: "Bearer " + server_access_token
    }
  }).then(res => {
    set_config(res.data);
    return config;
  }).catch(e=>{
    console.log('cannot connect to cofig server', e)
  })
};
