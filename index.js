const express = require('express');
const http = require('http');
const config = require('./config/keys.config');
const setup_controller_middleware = require('./middleware/setup_controller.middleware')
const stream = require('@algotechllc/tvdta').stream;


const app = express();
const server = http.createServer(app);
config().then(data=>{
    require('./config/DB.config')(data.database.auth,'charts')
    stream(server,data)
});
require('./middleware/common.middleware')(app);
app.use(setup_controller_middleware);
require("./config/router.config")(app);

const PORT = process.env.PORT || 5000;
server.listen(PORT)
console.log("server listning at port 5000")