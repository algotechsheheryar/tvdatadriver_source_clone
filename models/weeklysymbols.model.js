var mongoose = require("mongoose");

const csv_symbols_Schema = new mongoose.Schema(
  {
    _id: { type: String, required: false, ref: "Custom ID" },
    SYMBOL: {
      type: String,
      required: true
    },
    OPTION_NAME: {
      type: String,
      required: true
    },
    EFFECTIVE_DATE: {
      type: Number,
      required: true,
      ref: "mktValCap"
    }
  },
  { versionKey: false, collection: "weeklysymbols" }
);
module.exports = mongoose.model("weeklysymbols", csv_symbols_Schema);
