const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const user_schema = new Schema({
  created_at : {type: Date, default: Date.now()},
  first_name: { type: String, required: true },
  last_name: { type: String },
  username: { type: String, unique: true },
  email: { type: String, unique: true },
  account_type: { type: String, default: "live" },
  tradier_ID: { type: String, unique: true },
  tradier_access_token: { type: String },
  tradier_token_created_at: {type: Date, default: Date.now()},
  tradier_refresh_token: { type: String },
  password: { type: String },
  is_tradier_linked: { type: Boolean, default: false },
  is_completed: { type: Boolean, default: false },
  is_approved: { type: Boolean, default: true },
  is_verified: { type: Boolean, default: true },
  otp: { type: Number, default: Math.floor(Math.random() * 900000) }
});

module.exports = new mongoose.model("user", user_schema);

// abc = {
//   id: "id-test-partner",
//   name: "Test Partner",
//   accounts: [
//     {
//       account_number: "6YA05991",
//       classification: "individual",
//       date_created: "2016-08-01T21:08:55.000Z",
//       day_trader: false,
//       option_level: 6,
//       status: "active",
//       type: "margin",
//       last_update_date: "2016-08-01T21:08:55.000Z"
//     },
//     {
//       account_number: "6YA00005",
//       classification: "entity",
//       date_created: "2016-08-05T17:24:34.000Z",
//       day_trader: false,
//       option_level: 3,
//       status: "active",
//       type: "margin",
//       last_update_date: "2016-08-05T17:24:34.000Z"
//     },
//     {
//       account_number: "6YA05708",
//       classification: "rollover_ira",
//       date_created: "2016-08-01T21:08:56.000Z",
//       day_trader: false,
//       option_level: 2,
//       status: "active",
//       type: "cash",
//       last_update_date: "2016-08-01T21:08:56.000Z"
//     }
//   ],
//   provider: "tradier",
//   _raw:
//     '{"profile":{"account":[{"account_number":"6YA05991","classification":"individual","date_created":"2016-08-01T21:08:55.000Z","day_trader":false,"option_level":6,"status":"active","type":"margin","last_update_date":"2016-08-01T21:08:55.000Z"},{"account_number":"6YA00005","classification":"entity","date_created":"2016-08-05T17:24:34.000Z","day_trader":false,"option_level":3,"status":"active","type":"margin","last_update_date":"2016-08-05T17:24:34.000Z"},{"account_number":"6YA05708","classification":"rollover_ira","date_created":"2016-08-01T21:08:56.000Z","day_trader":false,"option_level":2,"status":"active","type":"cash","last_update_date":"2016-08-01T21:08:56.000Z"}],"id":"id-test-partner","name":"Test Partner"}}',
//   _json: {
//     profile: { account: [Array], id: "id-test-partner", name: "Test Partner" }
//   }
// };
