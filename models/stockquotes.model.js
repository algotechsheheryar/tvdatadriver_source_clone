var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var StockQuote = new Schema(
  {
    _id: { type: String, required: true, ref: "ID" },
    sb: { type: String, required: true, ref: "Symbol name" },
    dt: { type: Date, required: true, ref: "Date time" },
    o: { type: Number, required: true, ref: "Open" },
    h: { type: Number, required: true, ref: "High" },
    l: { type: Number, required: true, ref: "Low" },
    c: { type: Number, required: true, ref: "Close" },
    v: { type: Number, required: true, ref: "Volume" }
  },
  { versionKey: false }
);

var StockModel = mongoose.model("StockQuote", StockQuote);
module.exports = StockModel;
