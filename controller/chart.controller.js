const axios = require("axios");
const redis = require('redis');
const config = require('../config/keys.config')
const { json } = require("express");


exports.get_hello = (req,res)=>{ 
  res.send({"hello":"world"})
}
exports.get_historical_data = (req, res) => {
  const tradier_config = req.config.tradierToken;
  const { symbol, resolution } = req.query;

  if (!symbol.includes("*")) {
    let url;
    if (resolution == "1D" || resolution == "D")
      url = `${req.tradier_url}/markets/history?symbol=${symbol}&start=1970-01-01`;
    else if (resolution == 1 || resolution == 60) {
      var d = new Date();
      d.setDate(d.getDate() - 20);
      let date = `${d.getFullYear()}-${("0" + (d.getMonth() + 1)).slice(-2)}-${(
        "0" + d.getDate()
      ).slice(-2)}`;
      url = `${req.tradier_url}/markets/timesales?symbol=${symbol}&interval=1min&start=${date}`;
    }

    axios({
      method: "get",
      url: url,
      headers: {
        Authorization: "Bearer " + tradier_config
      }
    })
      .then(response => {
        let resData = response.data;
        client.set("tradierData",JSON.stringify(resData))
        if (resolution == "1D" || resolution == "D") {
          resData = resData.history.day;
        } else if (resolution == 1 || resolution == 60) {
          resData = resData.series.data;
          // });
        }
        res.json(resData);
      })
      .catch(e => {
        if (e.response) {
          if (e.response.data.fault) {
            res.status(400).json({
              tradier_token: e.response.data.fault.faultstring,
              tradier_error_code: e.response.data.fault.detail.errorcode
            });
          }
        }
      });
  } else {
    if (resolution == 1 || resolution == 60)
      return res
        .status(400)
        .json(
          "* symbol don't support interday data please change the resolution "
        );

    req.models.StockQuotesModel.find({ sb: symbol })
      .sort({ dt: 1 })
      .then(dd => {
        let data = dd.map(d => ({
          date: new Date(d.dt),
          open: d.o,
          high: d.h,
          low: d.l,
          close: d.c,
          volume: d.v
        }));
        const last_bar_date = data[data.length - 1].date;
        console.log(last_bar_date);        
        const d = new Date(last_bar_date);
        const start = `${d.getFullYear()}-${("0" + (d.getMonth() + 1)).slice(
          -2
        )}-${("0" + d.getDate()).slice(-2)}`;
        console.log('start->',start)
        console.log('just hitting tradier')
        axios({
          method: "get",
          url: `${req.tradier_url}/markets/history?symbol=${
            symbol.split("*")[0]
          }&start=${start}`,
          headers: {
            Authorization: "Bearer " + tradier_config
          }
        })
          .then(response => {
            // tradier = response;
            let pend_merge = response.data.history.day;
            console.log('got response from tradier')
            // pend_merge.splice(0, 1);
            data = [
              ...data,
              ...pend_merge.map(el => {
                let timestamp = new Date(
                  new Date(el.date).toLocaleString("en-US", {
                    timeZone: "America/New_York"
                  })
                );

                timestamp.setHours(40);
                //#region timeStamp Offset Comment
                /**Ideally, the set hrs should be 16 to force 16:00 EST; however there is a native error in this built-in function that sets the timestamp one data back, which is why we are                        adding a 24hr offset. */
                //#endregion

                timestamp = timestamp.getTime() / 1000;
                return {
                  date: new Date(timestamp * 1000),
                  open: el.open,
                  close: el.close,
                  high: el.high,
                  low: el.low,
                  volume: el.volume
                };
              })
            ];
            res.json(data);
          })
          .catch(e => {
            if (e.response) return res.status(400).json(e.response.data);
          });
        // res.json(data);
      });
  }
};

exports.get_all_symbols = (req, res) => {
  const tradier_config = req.config.tradierToken;
  // const optionalDB = req.config.optionalDB

  // req.models.WeeklySymbolsModel.find().then(sym=>res.json(sym))
  axios({
    method: "get",
    url: `${req.tradier_url}/markets/lookup?q=&exchanges=A,N,P,Q,Z&types=stock`,
    headers: {
      Authorization: "Bearer " + tradier_config
    }
  })
    .then(response => {
      let syms = [...response.data.securities.security];
      req.models.WeeklySymbolsModel.find().then(sym => {
        sym.forEach(s => {
          syms.push({
            symbol: s.SYMBOL + "*",
            description: s.OPTION_NAME,
            type: "stock",
            exchange: ""
          });
        });

        res.json(syms);
      });
      // res.json(syms);
    })
    .catch(e => {
      console.log(e);
      if (e.response) {
        if (e.response.data.fault) {
          res.status(400).json({
            tradier_token: e.response.data.fault.faultstring,
            tradier_error_code: e.response.data.fault.detail.errorcode
          });
        }
      }
    });
};
