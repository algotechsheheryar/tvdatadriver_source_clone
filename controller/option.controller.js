const axios = require("axios");

exports.get_expirations = async (req, res) => {
  try {
    const tradier_config = req.config.tradierToken;
    const symbol = req.query.symbol;
    axios({
      method: "get",
      url: `${req.tradier_url}/markets/options/expirations?symbol=${symbol}&includeAllRoots=true`,
      headers: {
        Authorization: "Bearer " + tradier_config
      }
    })
      .then(response => res.status(200).json(response.data.expirations.date))
      .catch(e => res.json(e));
  } catch (error) {
    throw new Error(error);
  }
};

exports.get_strikes = async (req, res) => {
  try {
    const tradier_config = req.config.tradierToken;
    const { symbol, expiration, range, near } = req.query;
    axios({
      method: "get",
      url: `${req.tradier_url}/markets/options/strikes?symbol=${symbol}&expiration=${expiration}`,
      headers: {
        Authorization: "Bearer " + tradier_config
      }
    })
      .then(response => {
        if (range) {
          if (near) {
            let g_strikes = response.data.strikes.strike
              .filter(s => s > near)
              .sort((a, b) => a - b)
              .splice(0, range / 2);
            let s_strikes = response.data.strikes.strike
              .filter(s => s < near)
              .sort((a, b) => a - b)
              .splice(-(range / 2), range / 2);
            const strikes = [...s_strikes, ...g_strikes].sort((a, b) => a - b);
            res.status(200).json(strikes);
          } else {
            let strikes = response.data.strikes.strike.splice(0, range);
            res.status(200).json(strikes);
          }
        } else {
          let strikes = response.data.strikes.strike;
          res.status(200).json(strikes);
        }
      })
      .catch(e => res.status(400).json(e.response));
  } catch (error) {
    res.status(400).json(e.response);
  }
};

exports.get_options_chain = async (req, res) => {
  try {
    const tradier_config = req.config.tradierToken;
    let {
      symbol,
      expiration,
      greeks,
      strike,
      strikes,
      option_type
    } = req.query;
    if (!symbol || !expiration)
      return res.status(400).json({
        error:
          "invalid symbol or expiration or bad symbol/expiration combination"
      });

    if (!greeks) greeks = false;

    axios({
      method: "get",
      url: `${req.tradier_url}/markets/options/chains?symbol=${symbol}&expiration=${expiration}&greeks=${greeks}`,
      headers: {
        Authorization: "Bearer " + tradier_config
      }
    })
      .then(response => {
        if (!strike && !strikes)
          res.status(200).json(response.data.options.option);
        else {
          if (!option_type) {
            if (strike) {
              const optt = response.data.options.option.filter(
                opt => opt.strike == strike
              );
              res.status(200).json(optt);
            } else if (strikes) {
              const str = strikes.split(",");
              const optt = [];
              response.data.options.option.forEach(o => {
                str.map(st => {
                  if (st == o.strike) return optt.push(o);
                });
              });
              res.status(200).json(optt);
            }
          } else
            res
              .status(200)
              .json(
                response.data.options.option.filter(
                  opt => opt.strike == strike && opt.option_type === option_type
                )
              );
        }
      })
      .catch(e => res.status(400).json(e.response));
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
};

exports.get_interest_rate_variable = (req, res) => {
  try {
    const series = req.query.series;
    const headers = {
      Authorization: "Bearer cbcb9eab416a92150fb4547b897efa79",
      Accept: "application/json"
    };

    //api params
    const params = {
      series_id: series,
      file_type: "json",
      sort_order: "desc",
      limit: 1,
      api_key: "cbcb9eab416a92150fb4547b897efa79"
    };

    //call to api
    axios
      .get("https://api.stlouisfed.org/fred/series/observations", {
        params: params,
        headers: headers
      })
      .then(response => {
        if (response)
          return res.status(200).json(response.data.observations[0].value);
        // throw new Error(response.error);
      })
      .catch(e => res.status(400).json(e.response));

    //check for responsev
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
};
