const axios = require("axios");
// const User = require("../models/user.model");
const bcrypt = require("bcrypt");
const { create_auth_token, pass_hash } = require("./../helper/auth.helper");
const complete_profile_validator = require("./../validations/complete_profile.validation");
const login_validator = require("./../validations/login.validation");

exports.tradier_callback_handler = (req, res) => {
  res.redirect(
    `http://localhost:3000/get_tradier?token=${req.user.accessToken}&name=${req.user.name}&id=${req.user.id}&refreshToken=${req.user.refreshToken}`
  );
};

exports.register_tradier = (req, res) => {
  const { id, name, token, refreshToken } = req.body;
  const { is_Authenticated, _id } = req.query;
  const User = req.models.UserModel

  if ((is_Authenticated, _id)) {
    User.findOneAndUpdate(
      { _id: _id },
      {
        tradier_ID: id,
        tradier_access_token: token,
        is_tradier_linked: true,
        tradier_refresh_token: refreshToken,
        tradier_token_created_at: Date.now()
      }
    )
      .then(user => {
        const token = create_auth_token({ _id: user._id }, "secret", 3600);
        res.status(200).json(`Bearer ${token}`);
      })
      .catch(e => console.log(e.message));
  } else {
    User.findOne({ tradier_ID: id }).then(user => {
      if (user) {
        const token = create_auth_token({ _id: user._id }, "secret", 3600);
        res.status(200).json(`Bearer ${token}`);
      } else {
        const new_user = new User({
          first_name: name.split(" ")[0],
          tradier_ID: id,
          tradier_access_token: token,
          is_tradier_linked: true,
          tradier_refresh_token: refreshToken,
          tradier_token_created_at: Date.now()
        });
        new_user.save().then(user => {
          const token = create_auth_token({ _id: user._id }, "secret", 3600);
          res.status(200).json(`Bearer ${token}`);
        });
      }
    });
  }
};

exports.get_user = (req, res) => {
  const User = req.models.UserModel
  if (req.user.hasOwnProperty("err"))
    return res.status(400).json({ token: `invalid session` });
  let user = { ...req.user._doc };
  const tradier_url =
    user.account_type === "paper"
      ? "https://sandbox.tradier.com/v1/user/profile"
      : "https://api.tradier.com/v1/user/profile";
  axios({
    method: "get",
    url: tradier_url,
    headers: {
      Authorization: "Bearer " + user.tradier_access_token
    }
  })
    .then(response => {
      const {
        _id,
        first_name,
        last_name,
        username,
        email,
        account_type,
        tradier_ID,
        tradier_access_token,
        tradier_refresh_token,
        is_tradier_linked,
        is_completed,
        is_approved,
        is_verified
      } = user;
      const new_user = {
        _id,
        first_name,
        last_name,
        username,
        email,
        account_type,
        tradier_ID,
        tradier_access_token,
        tradier_refresh_token,
        is_tradier_linked,
        is_completed,
        is_approved,
        is_verified
      };
      if (res)
        new_user.tradier_accounts = Array.isArray(response.data.profile.account)
          ? response.data.profile.account
          : [response.data.profile.account];
      res.status(200).json(new_user);
    })
    .catch(e => {
      const {
        _id,
        first_name,
        last_name,
        username,
        email,
        account_type,
        tradier_ID,
        tradier_access_token,
        tradier_refresh_token,
        is_tradier_linked,
        is_completed,
        is_approved,
        is_verified
      } = user;
      res.status(200).json({
        _id,
        first_name,
        last_name,
        username,
        email,
        account_type,
        tradier_ID,
        tradier_access_token,
        tradier_refresh_token,
        is_tradier_linked,
        is_completed,
        is_approved,
        is_verified,
        tradier_accounts: []
      });
    });
};

exports.login = (req, res) => {
  console.log(req.models)
  const User = req.models.UserModel
  console.log("abc",User, "abc")
  const { errors, is_valid } = login_validator(req.body);
  if (!is_valid) return res.status(400).json(errors);

  User.findOne({ username: req.body.username }).then(user => {
    if (!user) return res.status(400).json({ username: "user not exist" });
    const pass_match = bcrypt.compareSync(req.body.password, user.password);
    if (!pass_match)
      return res.status(400).json({ password: "Invalid password" });

    const token = create_auth_token({ _id: user._id }, "secret", 3600);
    res.status(200).json(`Bearer ${token}`);
  });
};

exports.register_or_complete = (req, res) => {
  const User = req.models.UserModel
  const _id = req.params.id ? req.params.id : false;
  const { errors, is_valid } = complete_profile_validator(req.body);

  if (!is_valid) return res.status(400).json(errors);
  ``;

  User.findOne({ email: req.body.email }).then(user => {
    if (user)
      return res
        .status(400)
        .json({ email: " account with this email already exist" });

    User.findOne({ username: req.body.username }).then(user => {
      if (user)
        return res
          .status(400)
          .json({ email: "username already exist try a different username" });

      let {
        first_name,
        last_name,
        email,
        username,
        password,
        account_type
      } = req.body;
      password = pass_hash(password);
      if (_id) {
        User.findOneAndUpdate(
          { _id, _id },
          {
            first_name,
            last_name,
            email,
            username,
            password,
            account_type,
            is_completed: true
          },
          { new: true, upsert: true, useFindAndModify: false }
        ).then(user => {
          const {
            _id,
            first_name,
            last_name,
            username,
            email,
            account_type,
            tradier_ID,
            tradier_access_token,
            tradier_refresh_token,
            is_tradier_linked,
            is_completed,
            is_approved,
            is_verified
          } = user;
          res.status(200).json({
            _id,
            first_name,
            last_name,
            username,
            email,
            account_type,
            tradier_ID,
            tradier_access_token,
            tradier_refresh_token,
            is_tradier_linked,
            is_completed,
            is_approved,
            is_verified,
            tradier_accounts: []
          });
        });
      } else {
        const newuser = new User({
          first_name,
          last_name,
          email,
          username,
          password,
          account_type,
          is_completed: true
        });
        newuser.save().then(user => res.status(200).json("success"));
      }
    });
  });
};
