const axios = require("axios");

exports.get_qoute = (req, res) => {
  const { symbol, type, strike } = req.query;

  if (type != "stock" && type != "option")
    return res.status(400).json({
      error:
        "please specify the type of quote | provided type => " +
        type +
        " is " +
        Boolean(type != "stock" && type != "option")
    });

  const access_token = req.config.tradierToken;
  axios({
    method: "get",
    url: `${req.tradier_url}/markets/quotes?symbols=${symbol}`,
    headers: {
      Authorization: "Bearer " + access_token
    }
  })
    .then(response => {
      const quote = response.data.quotes.quote;
      if (type === "option" && quote) {
        axios({
          method: "get",
          url: `${req.tradier_url}/markets/options/strikes?symbol=${quote.root_symbol}&expiration=${quote.expiration_date}`,
          headers: {
            Authorization: "Bearer " + access_token
          }
        })
          .then(response => {
            quote["all_strikes"] = response.data.strikes.strike;
            axios({
              method: "get",
              url: `${req.tradier_url}/markets/options/expirations?symbol=${quote.root_symbol}`,
              headers: {
                Authorization: "Bearer " + access_token
              }
            })
              .then(response => {
                quote["all_expirations"] = response.data.expirations.date;
                res.status(200).json(quote);
              })
              .catch(e => {
                if (e.response) return res.status(400).json(e.response.data);

                res.status(400).json(e);
              });
          })
          .catch(e => {
            if (e.response) return res.status(400).json(e.response.data);

            res.status(400).json(e);
          });
      } else if (type === "stock" && quote) {
        res.status(200).json(quote);
      } else {
        res.status(400).json(e);
      }
    })
    .catch(e => {
      if (e.response) return res.status(400).json(e.response.data);

      res.status(400).json({ error: "invalid symbol" });
    });
};

exports.get_orders = (req, res) => {
  const demo_config = req.demo_tradier;
  axios({
    method: "get",
    url: `${req.tradier_url}/user/orders`,
    headers: {
      Authorization: "Bearer " + req.config.tradierToken
    }
  })
    .then(response => {
      res.status(200).json(response.data);
    })
    .catch(e => {
      res.json(e);
    });
};

exports.place_multileg_order = (req, res) => {
  const demo_config = req.demo_tradier;
  const { symbol, type, duration, price, side, preview } = req.query;

  let to_params =
    type === "market" || type === "even"
      ? { symbol, type, duration }
      : { symbol, type, duration, price };
  req.body.forEach((d, i) => {
    to_params[`option_symbol[${i}]`] = d.option_symbol;
    to_params[`side[${i}]`] = d.side + "_to_" + side;
    to_params[`quantity[${i}]`] = d.quantity;
  });
  let params = {
    class: "multileg"
  };
  params["preview"] = preview === "true" ? true : false;
  params = { ...params, ...to_params };
  let url = "";
  Object.keys(params).forEach((data, i) => {
    if (i === Object.keys(params).length - 1) {
      url += data + "=" + params[data];
    } else {
      url += data + "=" + params[data] + "&";
    }
  });
  axios({
    method: "post",
    url: `${req.tradier_url}/accounts/${req.account_no}/orders?${url}`,
    headers: {
      Authorization: "Bearer " + req.config.tradierToken
    }
  })
    .then(response => {
      res.json(response.data);
    })
    .catch(e => {
      console.log(e);
      res.status(400).json(e);
    });
};

exports.place_option_order = (req, res) => {
  const demo_config = req.demo_tradier;
  const {
    preview,
    symbol,
    option_symbol,
    side,
    quantity,
    type,
    duration,
    price,
    stop_price
  } = req.query;

  let url = `class=option`;
  if (preview === "true") url += `&preview=true`;
  url += `&symbol=${symbol}&option_symbol=${option_symbol}&side=${side}&quantity=${quantity}&type=${type}&duration=${duration}`;
  if (type === "limit") url += `&price=${price}`;
  if (type === "stop") url += `&stop=${stop_price}`;
  if (type === "stop_limit") url += `&price=${price}&stop=${stop_price}`;
  axios({
    method: "post",
    url: `${req.tradier_url}/accounts/${req.account_no}/orders?${url}`,
    headers: {
      Authorization: "Bearer " + req.config.tradierToken
    }
  })
    .then(response => {
      res.json(response.data);
    })
    .catch(e => {
      console.log(e.response.data);
      res.status(400).json(e);
    });
};



exports.place_equity_order = (req, res) => {
  const {
    preview,
    symbol,
    // option_symbol,
    side,
    quantity,
    type,
    duration,
    price,
    stop_price
  } = req.query;

  let url = `class=equity`;
  if (preview === "true") url += `&preview=true`;
  url += `&symbol=${symbol}&side=${side}&quantity=${quantity}&type=${type}&duration=${duration}`;
  if (type === "limit") url += `&price=${price}`;
  if (type === "stop") url += `&stop=${stop_price}`;
  if (type === "stop_limit") url += `&price=${price}&stop=${stop_price}`;
  axios({
    method: "post",
    url: `${req.tradier_url}/accounts/${req.account_no}/orders?${url}`,
    headers: {
      Authorization: "Bearer " + req.config.tradierToken
    }
  })
    .then(response => {
      res.json(response.data);
    })
    .catch(e => {
      console.log(e.response.data);
      res.status(400).json(e);
    });
};
