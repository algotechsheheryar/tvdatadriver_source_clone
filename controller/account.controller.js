const axios = require("axios");

//helper
let currency_formatter = new Intl.NumberFormat("en-US", {
  maximumFractionDigits: 2,
  style: "currency",
  currency: "USD"
});
let percentage_formatter = {
  format: value => value.toFixed(2) + "%"
};

exports.get_account_history = (req, res) => {
  const account_no = req.query.acct_number;
  const access_token = req.config.tradierToken;
  axios({
    method: "get",
    url: `${req.tradier_url}/accounts/${account_no}/history?page=${req.query.page}&limit=100`,
    headers: {
      Authorization: "Bearer " + access_token
    }
  })
    .then(response => {
      if (response.data.history == "null")
        return res.status(400).json({ history: "no more results" });
      res.status(200).json(response.data);
    })
    .catch(e => {
      if (e.response) return res.status(400).json(e.response.data);
      res.status(400).json(e);
    });
};

exports.get_account_orders = (req, res) => {
  const account_no = req.query.acct_number;
  const access_token = req.config.tradierToken;
  console.log(req.tradier_url, account_no, access_token)
  axios({
    method: "get",
    url: `${req.tradier_url}/accounts/${account_no}/orders`,
    headers: {
      Authorization: "Bearer " + access_token
    }
  })
    .then(response => {
      if (response.data.orders == "null")
        return res.status(400).json({ order: "no more results" });
      res.status(200).json(response.data);
    })
    .catch(e => {
      // console.log(e.response)
      if (e.response) return res.status(400).json(e.response.data);
      res.status(400).json(e);
    });
};

exports.get_account_gain_loss = (req, res) => {
  const account_no = req.query.acct_number;
  const access_token = req.config.tradierToken;
  axios({
    method: "get",
    url: `${req.tradier_url}/accounts/${account_no}/gainloss?page=${req.query.page}&limit=100`,
    headers: {
      Authorization: "Bearer " + access_token
    }
  })
    .then(response => {
      if (response.data.gainloss == "null")
        return res.status(400).json({ gain_loss: "no more results" });
      res.status(200).json(response.data);
    })
    .catch(e => {
      if (e.response) return res.status(400).json(e.response.data);
      res.status(400).json(e);
    });
};


exports.get_acount_position = (req, res) => {
  const account_no = req.query.acct_number;
  const access_token = req.config.tradierToken;

  //getting positions
  axios({
    method: "get",
    url: `${req.tradier_url}/accounts/${account_no}/positions`,
    headers: {
      Authorization: "Bearer " + access_token
    }
  })
    .then(response => {
      // check if positions are not empty
      if (response.data.positions == "null")
        return res.status(400).json({ positions: "no results", res:response.data });

      // defining positions
      let positions = [];
      positions = [...response.data.positions.position];
      // let positions = [...dummy_positions];
      // console.log(positions);
      // extracting symbols from positions
      const all_symbols = positions.map(d => d.symbol);
      console.log(all_symbols.join(","));
      //getting qoutes for all symbols
      axios({
        method: "get",
        url: `${req.tradier_url}/markets/quotes?symbols=${all_symbols.join(
          ","
        )}`,
        headers: {
          Authorization: "Bearer " + access_token
        }
      })
        .then(response => {
          // console.log(response.data)
          // checking is qoute is array or obj
          let quotes = Array.isArray(response.data.quotes.quote)
            ? response.data.quotes.quote
            : [response.data.quotes.quote];

          // getting root symbols for option qoutes
          let ul_syb = quotes
            .filter(q => {
              const existed = all_symbols.find(a => {
                return a === q.root_symbol && q.type === "option";
              });
              return q.type == "option" && !Boolean(existed);
            })
            .map(q => q.root_symbol);

          ul_syb = ul_syb.length ? removeDups(ul_syb).join(",") : "AAPL";

          // geting stock qoutes of the root symbols of option qoutes
          axios({
            method: "get",
            url: `${req.tradier_url}/markets/quotes?symbols=${ul_syb}`,
            headers: {
              Authorization: "Bearer " + access_token
            }
          })
            .then(response => {
              quotes = Array.isArray(response.data.quotes.quote)
                ? [...quotes, ...response.data.quotes.quote]
                : [...quotes, response.data.quotes.quote];

              // pouring details in to positions using qoutes
              const u_positions = positions.map(pos => {
                let post = { ...pos };

                // finding qoute for current position
                const quote = quotes.find(q => q.symbol === pos.symbol);

                /**
                 * stock last is last of qoute is position is stock type otherwise it would be the last price of stock of the root symbol of underlying symbol of current option qoute
                 * **/
                const stock_last =
                  quote.type === "stock"
                    ? quote.last
                    : quotes.find(q => q.symbol === quote.root_symbol).last;

                const last = quote.last;

                // value => (if qty of position < 0 then ask of current qoute else bid)*qty
                const value = pos.quantity < 0 ? quote.ask* pos.quantity : quote.bid*pos.quantity;

                /**
                 * pnl=>
                 * if qoute type is option then
                 * (value * 100 )- (cost basis of position)
                 *
                 * if qoute type is stock then
                 *  if position qty < 0
                 *    Cost Basis - value
                 * if position qty > 0
                 *   value - Cost Basis
                 */
                const pnl =
                  quote.type === "stock"
                    ? pos.quantity < 0
                      ? pos.cost_basis - value 
                      : value - pos.cost_basis
                    : value * 100 - pos.cost_basis;

                /**
                 *  pnl % => ( pnl / cost basis ) * 100
                 *
                 * note =>   if quote type is option then
                 *      if qty < 1 then multiply pnl % with -1 (pnl_percent * -1)
                 */
                let pnl_percent = (pnl / pos.cost_basis) * 100;
                pnl_percent =
                  quote.type === "stock"
                    ? pnl_percent
                    : pos.quantity < 0
                    ? pnl_percent * -1
                    : pnl_percent;

                // net change
                const net_change =
                  quote.type === "stock"
                    ? quote.last - pos.cost_basis
                    : quote.last - pos.cost_basis / 100;
                //net change %
                const net_change_percent =
                  quote.type === "stock"
                    ? (net_change / pos.cost_basis) * 100
                    : (net_change / (pos.cost_basis / 100)) * 100;

                const is_expiring =
                  quote.type === "option"
                    ? (new Date(quote.expiration_date) - Date.now()) /
                        (24 * 3600 * 1000) <
                      1
                      ? true
                      : false
                    : false;

                /**
                 * 		OTM Put -> If option_type = Put Then
                 *      If LastPrice of Underlying > Strike Then OTM
                 *    OTM Call -> If option_type = Call Then
                 *      If LastPrice of Underlying < Strike, Then OTM
                 */
                const otm =
                  quote.type == "option"
                    ? quote.option_type === "put"
                      ? stock_last > quote.strike
                      : stock_last < quote.strike
                    : false;

                /**
                 *    ITM Put -> If option_type = Put Then
                 * 			If LastPrice of Underlying < Strike Then ITM
                 *    ITM Call -> If option_type = Call Then
                 *      If LastPrice of Underlying > Strike, Then ITM
                 */
                const itm =
                  quote.type == "option"
                    ? quote.option_type === "call"
                      ? stock_last > quote.strike
                      : stock_last < quote.strike
                    : false;

                post = {
                  ...post,
                  description: quote.description,
                  ul_symbol:
                    quote.type === "option" ? quote.root_symbol : quote.symbol,
                  last: currency_formatter.format(last),
                  stock_last,
                  type: quote.type,
                  value: currency_formatter.format(value),
                  pnl: currency_formatter.format(pnl),
                  pnl_percent: percentage_formatter.format(pnl_percent),
                  net_change: net_change.toFixed(2),
                  net_change_percent: net_change_percent.toFixed(2),
                  is_expiring,
                  ask: quote.ask,
                  bid: quote.bid,
                  //spread ask - bid
                  spread: quote.ask-quote.bid,
                  otm,
                  itm
                };
                return post;
              });
              res.status(200).json(u_positions);
            })
            .catch(e => {
              if (e.response) return res.status(400).json(e.response.data);
              res.status(400).json(e);
            });
        })
        .catch(e => {
          if (e.response) return res.status(400).json(e.response.data);
          res.status(400).json(e);
        });
    })
    .catch(e => {
      if (e.response) return res.status(400).json(e.response.data);
      res.status(400).json(e);
    });
};

exports.get_ul_symbols = (req, res) => {
  let all_symbols = req.body;

  const access_token = req.config.tradierToken;
  axios({
    method: "get",
    url: `${req.tradier_url}/markets/quotes?symbols=${all_symbols.join(",")}`,
    headers: {
      Authorization: "Bearer " + access_token
    }
  })
    .then(response => {
      const quotes = Array.isArray(response.data.quotes.quote)
        ? response.data.quotes.quote
        : [response.data.quotes.quote];
      let ul_symbol = quotes.map(q => {
        return q.type !== "option" ? q.symbol : q.root_symbol;
      });
      ul_symbol = removeDups(ul_symbol);
      res.status(200).json(ul_symbol);
    })
    .catch(e => {
      if (e.response) res.status(400).json("err");
    });
};

// helper

function removeDups(names) {
  let unique = {};
  names.forEach(function(i) {
    if (!unique[i]) {
      unique[i] = true;
    }
  });
  return Object.keys(unique);
}
