const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

exports.create_auth_token = (payload, secret, expiry) => {
  const token = jwt.sign(payload, secret);
  return token;
};

exports.pass_hash = password => {
  const hash = bcrypt.hashSync(password, 10);
  return hash;
};
