const jwt_decode = require("jwt-decode");
const User = require("../models/user.model");
const get_user_from_token =   B_token => {
  const token = B_token.split(" ")[1];
  const decoded = jwt_decode(token);
  // const user = await User.findById(decoded._id);
  return  decoded._id;
};

module.exports = get_user_from_token
