const validator = require("validator");
const is_Empty = require("./is_empty");

module.exports = data => {
  let errors = {};
  let { username, password } = data;
  username = !is_Empty(username) ? username : "";
  password = !is_Empty(password) ? password : "";

  // validating username
  if (validator.isEmpty(username)) {
    errors.username = "username is required";
  }

  // validating password
  if (validator.isEmpty(password)) {
    errors.password = "password is required";
  }

  return {
    errors,
    is_valid: is_Empty(errors)
  };
};
