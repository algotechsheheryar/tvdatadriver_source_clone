const validator = require("validator");
const is_Empty = require("./is_empty");

module.exports = data => {
  let errors = {};
  let { first_name, last_name, username, email, password } = data;
  first_name = !is_Empty(first_name) ? first_name : "";
  last_name = !is_Empty(last_name) ? last_name : "";
  username = !is_Empty(username) ? username : "";
  email = !is_Empty(email) ? email : "";
  password = !is_Empty(password) ? password : "";

  // validating first_name
  if (validator.isEmpty(first_name)) {
    errors.first_name = "first name is required";
  }
  if (!validator.isLength(first_name, { min: 2, max: 10 })) {
    errors.first_name =
      "first name should be at least minimum of 2 char to 10 char long";
  }

  // validating last_name
  if (validator.isEmpty(last_name)) {
    errors.last_name = "last name is required";
  }
  if (!validator.isLength(last_name, { min: 2, max: 10 })) {
    errors.last_name =
      "last name should be at least minimum of 2 char to 10 char long";
  }

  // validating username
  if (validator.isEmpty(username)) {
    errors.username = "username is required";
  }
  if (!validator.isLength(username, { min: 2, max: 10 })) {
    errors.username =
      "username should be at least minimum of 2 char to 10 char long";
  }

  // validating email
  if (validator.isEmpty(email)) {
    errors.email = "email is required";
  }
  if (!validator.isEmail(email)) {
    errors.email = "Please enter a valid Email";
  }

  // validating password
  if (validator.isEmpty(password)) {
    errors.password = "password is required";
  }
  if (!validator.isLength(password, { min: 6, max: 20 })) {
    errors.password = "password should be atleast 6 to 20 char long";
  }
  const password_regex = new RegExp(
    "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})"
  );
  if (!password_regex.test(password)) {
    errors.password =
      "password must contain at least one upper case letter one lower case letter one number and one special character ";
  }

  return {
    errors,
    is_valid: is_Empty(errors)
  };
};
