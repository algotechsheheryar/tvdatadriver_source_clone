const router = require("express").Router();
// const chart_controller = require("@algotechllc/tvdta").controller;
const chart_controller = require("../controller/chart.controller")
const passport = require('passport')

router.get("/historical_data", chart_controller.get_historical_data);

router.get("/all_symbols", chart_controller.get_all_symbols);
router.get("/helloworld", chart_controller.get_hello);
module.exports = router;
