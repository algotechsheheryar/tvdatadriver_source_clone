const router = require("express").Router();
const controller = require("./../controller/account.controller");


router.get("/get_history", controller.get_account_history);

router.get("/get_orders", controller.get_account_orders);

router.get("/get_gain_loss", controller.get_account_gain_loss);

router.get("/get_positions", controller.get_acount_position);

router.post('/get_ul_symbols', controller.get_ul_symbols)

module.exports = router;
