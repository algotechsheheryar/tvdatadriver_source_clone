const router = require("express").Router();
const passport = require("passport");
// const controller = require("./../controller/auth.controller");
const controller = require("@algotechllc/authcontroller/lib/index");


router.post("/login", controller.login);

router.post("/register", controller.register_or_complete);

router.post("/complete_profile/:id", controller.register_or_complete);

router.get(
  "/tradier",
  passport.authenticate("tradier", {
    scope: ["read", "write", "market", "trade", "stream"]
  })
);

router.get(
  "/tradier/callback",
  passport.authenticate("tradier", { failureRedirect: "/login" }),
  controller.tradier_callback_handler
);

router.post('/register_tradier', controller.register_tradier)

router.get(
  "/me",
  passport.authenticate("jwt", { session: false }),
  controller.get_user
);

module.exports = router;
