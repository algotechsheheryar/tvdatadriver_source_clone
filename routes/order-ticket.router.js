const router = require("express").Router();
const controller = require("./../controller/order-ticket.controller");

// router.get('/get_account_position', controller.get_account_position)

// router.get('/get_account_balance', controller.get_account_balance)

// router.get('/get_orders', controller.get_orders)

router.post("/place_multileg_order", controller.place_multileg_order);

router.post("/place_option_order", controller.place_option_order);

router.get("/get_quotes", controller.get_qoute);

router.post('/place_equity_order', controller.place_equity_order)

// router.post("/preview_multileg_order", controller.place_multileg_order)

module.exports = router;
