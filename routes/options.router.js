const router = require("express").Router();
// const controller = require('./../controller/options.controller')
// const controller = require("@algotechllc/optionschainapis")
const controller = require("./../controller/option.controller")

router.get('/get_expirations', controller.get_expirations)

router.get('/get_strikes', controller.get_strikes)

router.get("/get_options_chain", controller.get_options_chain)

router.get("/get_interest_rate_variable", controller.get_interest_rate_variable)


// router.get("/chains", controller.getOptionData)

module.exports = router;
